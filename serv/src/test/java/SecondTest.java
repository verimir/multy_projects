import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.testng.annotations.Test;
@Epic("AT")
@Feature("second test class")
public class SecondTest {
    @Test(description = "second test method")
    public void secondTest(){
        sayMyName("This is Sparta");
    }

    @Step("say my name step")
    private void sayMyName(String name){
        System.out.println(name);
    }
}
